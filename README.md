# Zima SDK Demo

Zima SDK是一个力求轻量模块化可移植的2D激光SLAM导航家用清洁机器人算法SDK。

**目前仅供学习交流，禁止商用。**

SDK包含基础数据格式、控制算法、规划算法、SLAM算法，低依赖（目前只需glog/gflags/protobuf）。为便于调试，也加入了ros封装和简单gazebo仿真。

目前闭源Gazebo仿真Demo部署方式为docker。(Demo已内置里程计的累计误差模拟，和雷达的误差模拟)

更新日志：

v0.1.0
1. 支持规划清扫、暂停、重定位继续清扫。
1. 支持地图保存（3张），选择地图进行二次清扫。
1. 快速建图（实验阶段）。

v0.1.1
1. 修复快速建图Bug。
1. 修复重定位失效Bug。

Demo方法（示例宿主机为Ubuntu22.04系统，理论上可兼容其他Linux发行版）：

宿主机需要先安装Docker，并拉取镜像：
```bash
docker pull zimatec/ros:zima-demo-v0.1.1
```

容器创建启动方式：
```bash
if [ -e /dev/nvidia0 ]; then
  echo "Launch with nvidia support."
  docker run \
    -it \
    -u zima \
    --name="zima_demo" \
    --net=host \
    --privileged \
    -v /dev:/dev \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --runtime=nvidia \
    --device /dev/nvidia0 \
    --device /dev/nvidia-uvm \
    --device /dev/nvidia-uvm-tools \
    --device /dev/nvidiactl \
    --runtime=nvidia \
    --gpus all \
    zimatec/ros:zima-demo-v0.1.1
else
  echo "Launch without nvidia support."
  docker run \
    -it \
    -u zima \
    --name="zima_demo" \
    --net=host \
    --privileged \
    -v /dev:/dev \
    -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    zimatec/ros:zima-demo-v0.1.1
fi
```

容器中启动仿真环境方法(在独立终端中运行)：
```bash
roslaunch zima_gazebo gazebo.launch
```
![gazebo](assets/5.png)

容器中启动Demo(在独立终端中运行)：
```bash
roslaunch zima_ros gazebo_demo.launch
```
![gazebo](assets/6.png)

容器中启动Rviz(在独立终端中运行)：
```bash
roslaunch zima_ros rviz.launch
```
![gazebo](assets/2.png)

建议仿真环境与Demo与Rviz从不同的终端窗口进入docker后启动，因为Demo程序使用键盘标准输入为测试命令输入，用一个launch文件一起启动的话键盘输入会失效。

键盘控制详细请看Demo程序输出提示，若提示日志已被刷走，可按esc键或任意非功能键来输出提示。
